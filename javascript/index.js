

/**********************************************************************************
 *
 *		GLOBAL VARIABLES
 *
 *********************************************************************************/


var net = require("net");
var JSONStream = require('JSONStream');

var logTimes = false;
var startTime = process.hrtime();
var client;
var jsonStream;
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];


/**********************************************************************************
 *
 *		INITIALIZING
 *
 *********************************************************************************/


function init() {
	console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

	client = net.connect(serverPort, serverHost, function () {
        
		return send({
			msgType: "join",
			data: {
				name: botName,
				key: botKey
			}
		});
        
        /*
	    return send({"msgType": "joinRace", "data": {
	        "botId": {
	            "name": botName,
	            "key": botKey
	        },
	        "trackName": "germany"
	        //"carCount": 3
	    }});
        */
	});

	jsonStream = client.pipe(JSONStream.parse());

	jsonStream.on('data', receiveData);

	jsonStream.on('error', function() {
		return console.log("disconnected");
	});
}

/**********************************************************************************
 *
 *		SENDING & RECEIVING
 *
 *********************************************************************************/

function send(json) {
	client.write(JSON.stringify(json));
	return client.write('\n');
};

function receiveData(data) {
	//quicklog.write(data);

    switch (data.msgType) {
        case "join":
            logic.join(data);
            break;
        case "joinRace":
            logic.joinRace(data);
            break;
        case "yourCar":
            logic.yourCar(data);
            break;
        case "gameInit":
            logic.gameInit(data);
            break;
        case "gameStart":
            logic.gameStart(data);
            break;
        case "carPositions":
            logic.carPositions(data);
            break;
        case "lapFinished":
            logic.lapFinished(data);
            break;
        case "crash":
            logic.crash(data);
            break;
        case "finish":
            logic.finish(data);
            break;
        case "gameEnd":
            logic.gameEnd(data);
            break;
        case "tournamentEnd":
            logic.tournamentEnd(data);
            break;
        case "dnf":
            logic.dnf(data);
            break;
        case "turboAvailable":
            logic.turboAvailable(data);
            break;
    }
}


/**********************************************************************************
 *
 *		LOGIC
 *
 *********************************************************************************/


var logic = (function() {

	function join(Join) {
		console.log("Joined");

		send({
			msgType: "ping",
			data: {}
		});
	}

	function joinRace(JoinRace) {
		console.log("Joined");

		send({
			msgType: "ping",
			data: {}
		});
	}
	
	function yourCar(YourCar) {
		console.log("Car data received: "
			+YourCar.data.name+" color is "+YourCar.data.color);

		cars.setBotColor(YourCar.data.color);
	}
	
	function gameInit(GameInit) {
		console.log("Game data received");

		track.setTrackData(GameInit.data.race.track);
		cars.setCarsData(GameInit.data.race.cars);
	}
	
	function gameStart(GameStart) {
		console.log("Game started");

		send({
			msgType: "ping",
			data: {}
		});
	}
	
	function carPositions(CarPositions) {
		if(CarPositions.gameTick % 50 === 0) {
			console.log("Gametick: "+CarPositions.gameTick);
			//quicklog.write("Gametick: "+CarPositions.gameTick);
		}

		cars.setBotPositions(CarPositions.data);
		if (!raceLogic.checkSwitchAction(CarPositions)) {
		    raceLogic.ajustSpeed(CarPositions);
		}
	}

	function lapFinished(LapFinished) {
		console.log("Lap "+LapFinished.data.raceTime.laps+" finished");
	}
	
	function crash(Crash) {
	    console.log("You crashed!!! >:(");

	    track.saveCrashReport();
	}

	function finish(Finish) {
		console.log("Race finished");
	}

	function gameEnd(GameEnd) {
		console.log("Race ended");

		send({
			msgType: "ping",
			data: {}
		});
	}

	function tournamentEnd(TournamentEnd) {
		console.log("Tournament ended");

		//quicklog.close();
	}

	function dnf(Dnf) {
		console.log(Dnf.data.car.name+" "+Dnf.data.car.color+" disqualified :"
			+Dnf.data.reason);
		//quicklog.close();
	}

	function turboAvailable(TurboAvailable) {
	    console.log("Turbo available!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! :D");

	    cars.setBotTurboAvailable(TurboAvailable);
	}

	return {
		join: 			join,
		joinRace:		joinRace,
		yourCar: 		yourCar,
		gameInit: 		gameInit,
		gameStart: 		gameStart,
		carPositions: 	carPositions,
		lapFinished: 	lapFinished,
		crash: 			crash,
		finish: 		finish,
		gameEnd: 		gameEnd,
		tournamentEnd: 	tournamentEnd,
		dnf:            dnf,
		turboAvailable: turboAvailable
	}
})()


var raceLogic = (function() {

    var latestSwitchActionIndex = -1;

    function throttle(howMuch) {
        send({
            msgType: "throttle",
            data: howMuch
        });

        cars.setBotsCurrentThrottle(howMuch);

        console.log('Throttle: ' + howMuch);
    }

    function useTurbo() {
        console.log("****************** TURBO USED !!!!!!!!!!!!!!!!!!!");

        send({
            msgType: "turbo",
            data: "w00t"
        });

        cars.setBotTurboAvailable(null);
    }

    function throttleToSpeed(targetSpeed) {
        console.log('Throttle to speed: ' + targetSpeed);

        //   10        10                  0
        //   -10       0                  10
        var diff = targetSpeed - cars.getBotsCurrentSpeed();
        var thr = cars.getBotsCurrentThrottle();

        // Target speed larger than current speed (assume max speed 10 units per tick, maybe shouldn't?)
        if (diff > 3) {
            thr = 1;
        } else if(diff != 0) {
            thr = diff / 2;
        }

        if(thr > 1) {
            thr = 1;
        }

        if(thr < 0) {
            thr = 0;
        }
        
        throttle(thr);
    }

    function checkSwitchAction(CarPositions) {
        if (track.isNextPieceSwitch() === true && latestSwitchActionIndex !== cars.getBotsCurrentPieceIndex() + 1) {

            latestSwitchActionIndex = cars.getBotsCurrentPieceIndex() + 1;
            var switchAction = selectSwitchAction();

            if (switchAction !== "") {
                console.log("Sending action: " + switchAction);
                send({
                    msgType: "switchLane",
                    data: switchAction
                });

                return true;
            }
        }

        return false;
    }

    // TODO: if opponets are ahead, then don't switch for the shortest (they'll probably do it also). Instead, try to overtake at the longer lane?
    function selectSwitchAction() {
        //TODO: can there be two switches in a row???? think about that
        var routeStart = cars.getBotsCurrentPieceIndex() + 2;
        var routeEnd;
        var iterator = routeStart;
        do {
            routeEnd = iterator;
            iterator++;
            if(iterator > track.getLastPieceIndex()) {
                iterator = 0
            }
        } while(track.isPieceSwitch(iterator) === false);

        var laneLengths = track.getRoutesLaneLengths(routeStart, routeEnd);
        var shortestLaneLength = Math.min.apply(Math, laneLengths);
        var shortestLaneIndex  = laneLengths.indexOf(shortestLaneLength);
        var switchDirection = "";

        if (shortestLaneIndex > cars.getBotsCurrentLaneIndex() && shortestLaneLength < laneLengths[cars.getBotsCurrentLaneIndex()]) {
            switchDirection = "Right";
        } else if (shortestLaneIndex < cars.getBotsCurrentLaneIndex() && shortestLaneLength < laneLengths[cars.getBotsCurrentLaneIndex()]) {
            switchDirection = "Left";
        } else {
            switchDirection = cars.shouldSwitchLane(cars.getBotsCurrentPieceIndex(), cars.getBotsCurrentLaneIndex());

            if (switchDirection !== "") {
                console.log('Trying to take over! w00t');
            }
        }

        return switchDirection;
    }

    // TODO: adjust speed according to the curve angle --> OK
    // TODO: make use of turbo (currently doesn't work)
    function ajustSpeed(CarPositions) {
        var angleAdjust;
        var maxSpeedAtCurve = 8;
        var curv = 0;
        var lastAngle = 0;
        var angleDelta = 0;

        if (cars.getBotsCurrentAngle() != 0) {
            console.log('Angle: ' + cars.getBotsCurrentAngle());
        }

        // If the angle is getting smaller, then we're survived the curve and can go full throttle again
        angleDelta = lastAngle - Math.abs(cars.getBotsCurrentAngle());
        lastAngle = Math.abs(cars.getBotsCurrentAngle());

        if (cars.getBotTurboAvailable() && track.isPieceStraight(cars.getBotsCurrentPieceIndex()) && track.getLengthOfNextStraightPieces() >= (300 + cars.getBotsCurrentSpeed() * 10)) {
            useTurbo();
        } else if (track.isNextPieceCurve() === true) {
            // If getting close to a curve

            curv = track.getCurveAdjustment(track.getNextPieceIndex());
            if(curv > 0)
                console.log('Curve adjust: ' + curv);

            // Is the speed safe?
            var safeSpeed = track.getCrashSpeedOfPiece(track.getNextPieceIndex(), cars.getBotsCurrentLaneIndex()) - curv - 3;
            if (safeSpeed < 0) {
                safeSpeed = 0;
            }

            if (cars.getBotsCurrentSpeed() > safeSpeed) {
                console.log('Previously crashed here, slowing down...');
                throttleToSpeed(0.1);
            } else {
                /*
                if (track.isPieceStraight(track.getNextPieceIndex())) {
                    // Is a hard turn coming?
                    //if (Math.abs(track.getAngleOfNextCurvePiece() > (35 - curv)) && angleDelta > 0) {
                    //    throttleToSpeed(9 - curv);
                    //} else {
                        throttleToSpeed(9);
                    //}
                } else {
                */
                    // 60 seems to be the magic number for crashing...
                    if (Math.abs(cars.getBotsCurrentAngle()) > (35 - curv) && angleDelta > 0) {
                        throttleToSpeed(4);
                    } else {
                        if (track.isPieceStraight(track.getNextPieceIndex())) {
                            throttleToSpeed(track.getLengthOfNextStraightPieces() / 100 + 9 - curv);
                        } else {
                            throttleToSpeed(9 - curv);
                        }
                    }
                //}
            }
        } else {
            curv = track.getCurveAdjustment(track.getNextPieceIndex());
            if(curv > 0)
                console.log('Curve adjust: ' + curv);

            if (cars.getBotsCurrentSpeed() > track.getCrashSpeedOfPiece(cars.getBotsCurrentPieceIndex(), cars.getBotsCurrentLaneIndex()) - curv - 3) {
                console.log('Previously crashed here, slowing down...');
                throttleToSpeed(1);
            } else {
                // Are we on a curve and the next piece is straight?
                if (!track.isPieceStraight(cars.getBotsCurrentPieceIndex())) {
                    // 60 seems to be the magic number for crashing...
                    if (Math.abs(cars.getBotsCurrentAngle()) > (35 - curv) && angleDelta > 0) {
                        throttleToSpeed(4);
                    } else {
                        throttleToSpeed(track.getLengthOfNextStraightPieces() / 100 + 9 - curv);
                    }
                } else {
                    // We're on a straight and the next piece is also a straight
                    throttleToSpeed(track.getLengthOfNextStraightPieces() / 100 + 9 - curv);
                }
            }
        }

        console.log('Current speed: ' + cars.getBotsCurrentSpeed());
    }

	return {
	    checkSwitchAction: checkSwitchAction,
	    ajustSpeed :       ajustSpeed
	}
})()


/**********************************************************************************
 *
 *		MODELS
 *
 *********************************************************************************/


var cars = (function() {
	var bot;
	var botColor;
	var opponents = {};
	var turbo = {};
	var turboAvailable = false;

	function setBotColor(color) {
		botColor = color;
	}

	function setCarsData(carsData) {
		for(var i = 0; i < carsData.length; i++) {
		    if (carsData[i].id.color === botColor) {
		        //this is our bot
		        bot = carsData[i];
		        bot.speed = 0;
		    } else {
		        //these are opponent bots
		        opponents[carsData[i].id.color] = carsData[i];
		        opponents[carsData[i].id.color].speed = 0;
		    }
		};
	}

	function setBotPositions(positions) {
	    calculateSpeeds(positions);

		for(var i = 0; i < positions.length; i++) {
		    if (positions[i].id.color === botColor) {
		        //this is our bot
		        bot.angle = positions[i].angle;
		        bot.piecePosition = positions[i].piecePosition;
		    } else {
		        //these are opponent bots
		        opponents[positions[i].id.color].angle = positions[i].angle;
		        opponents[positions[i].id.color].piecePosition = positions[i].piecePosition;
		    }
		};
	}

    /*
    * Calculates current speed per tick
    */
	function calculateSpeeds(positions) {
	    if (!bot.hasOwnProperty('piecePosition')) {
	        return;
	    }

	    for (var i = 0; i < positions.length; i++) {
	        if (positions[i].id.color === botColor) {
	            //this is our bot
	            var delta = positions[i].piecePosition.inPieceDistance - bot.piecePosition.inPieceDistance;

                // TODO: fix a bug that makes the delta occasionally negative (occurs when a piece changes?)
                if(delta >= 0) {
                    bot.speed = delta;
                }
	        } else {
	            //these are opponent bots
	            var delta = positions[i].piecePosition.inPieceDistance - opponents[positions[i].id.color].piecePosition.inPieceDistance;
	            if (delta >= 0) {
	                opponents[positions[i].id.color].speed = delta;
	            }
	        }
	    };
	}


    /*
    * Returns the lane if should switch.
    */
	function shouldSwitchLane(pieceIndex, laneIndex, direction) {
	    var shouldSwitch = "";

	    for (var i = 0; i < opponents.length; i++) {
	        // Opponent is on the same piece, same lane and ahead of us and opponent is not changing lane
	        if (opponents[i].piecePosition.lane.startLaneIndex === bot.piecePosition.lane.startLaneIndex && opponents[i].piecePosition.lane.startLaneIndex === opponents[i].piecePosition.lane.endLaneIndex && opponents[i].piecePosition.inPieceDistance > bot.piecePosition.inPieceDistance) {
	            if (opponents[i].piecePosition.lane.startLaneIndex > bot.piecePosition.lane.startLaneIndex) {
	                shouldSwitch = "Right";
	            } else {
	                shouldSwitch = "Left";
	            }

	            break;
	        }
	    }

	    return shouldSwitch;
	}

	function getBotsCurrentPieceIndex() {
		return bot.piecePosition.pieceIndex;
	}

	function getBotsCurrentLaneIndex() {
		return bot.piecePosition.lane.startLaneIndex;
	}

	function getBotsCurrentInPiecePosition() {
	    return bot.piecePosition.inPieceDistance;
	}

	function getBotsCurrentSpeed() {
	    return bot.speed;
	}

	function getBotsCurrentThrottle() {
	    return bot.throttle;
	}

	function setBotsCurrentThrottle(throttle) {
	    bot.throttle = throttle;
	}

	function getBotsCurrentAngle() {
	    return bot.angle;
	}

	function setBotTurboAvailable(turboData) {
	    if (turboData === null) {
	        turboAvailable = false;
	    } else {
	        bot.turbo = turboData;
	        turboAvailable = true;
	    }
	}

	function getBotTurboAvailable() {
	    return turboAvailable;
	}

	return {
		setBotColor:				setBotColor,
		setCarsData:				setCarsData,
		setBotPositions:			setBotPositions,
		getBotsCurrentPieceIndex:   getBotsCurrentPieceIndex,
		getBotsCurrentLaneIndex:    getBotsCurrentLaneIndex,
		getBotsCurrentInPiecePosition: getBotsCurrentInPiecePosition,
		getBotsCurrentSpeed:        getBotsCurrentSpeed,
		getBotsCurrentAngle:        getBotsCurrentAngle,
		getBotsCurrentThrottle:     getBotsCurrentThrottle,
		setBotsCurrentThrottle:     setBotsCurrentThrottle,
		setBotTurboAvailable:       setBotTurboAvailable,
		getBotTurboAvailable:       getBotTurboAvailable,
		shouldSwitchLane:           shouldSwitchLane
	}
})()


var track = (function() {
	var track;

	function setTrackData(trackData) {
		track = trackData;
	}

	function getTrackData() {
	    return track;
	}

	function isPieceStraight(pieceIndex) {
	    return (typeof track.pieces[pieceIndex].length === "number") ? true : false;
	}

	function isPieceSwitch(pieceIndex) {
	    return (typeof track.pieces[pieceIndex].switch === "boolean") ? true : false;
	}

	function getLastPieceIndex() {
		return (track.pieces.length - 1);
	}

    /*
    * Gets the number of consecutive straight pieces.
    *
    * Used to determine how much we can throttle at the current piece.
    */
	function getNumNextStraightPieces() {
	    var retval = 0;
	    var i = cars.getBotsCurrentPieceIndex();
	    var startPieceIndex = i;

	    do
	    {
	        if (typeof track.pieces[i].length === "number") {
	            retval++;
	        } else {
	            break;
	        }

	        if (i++ > getLastPieceIndex()) {
	            i = 0
	        }
	    } while(i !== startPieceIndex)

	    return retval;
	}

    /*
    * Gets the TOTAL LENGTH of consecutive straight pieces.
    *
    * Used to determine how much we can throttle at the current piece.
    */
	function getLengthOfNextStraightPieces() {
	    var i = getNextPieceIndex();
	    var retval = 0;
	    var startPieceIndex = i;

	    do {
	        if (typeof track.pieces[i].length === "number") {
	            retval += track.pieces[i].length;
	        } else {
	            break;
	        }

	        if (++i > getLastPieceIndex()) {
	            i = 0
	        }
	    } while (i !== startPieceIndex)

	    return retval;
	}

    /*
    * Gets the angle of the curved piece.
    */
	function getCurveAngle(pieceIndex) {
	    if (typeof track.pieces[pieceIndex].length === "number") {
	        return 0;
	    }

	    return track.pieces[pieceIndex].angle;
	}

	function getCurveAdjustment(pieceIndex) {
	    if (typeof track.pieces[pieceIndex].length === "number") {
	        return 0;
	    }

	    return ((1 / track.pieces[pieceIndex].radius) * 100) * 2;
	}

	function isNextPieceSwitch() {
	    var nextPieceIndex = getNextPieceIndex();

		if (typeof track.pieces[nextPieceIndex].switch === "boolean" && track.pieces[nextPieceIndex].switch === true) {
		    return true;
		} else {
		    return false;
		}
	}

	function isNextPieceCurve() {
	    var nextPieceIndex = getNextPieceIndex();

	    return (typeof track.pieces[nextPieceIndex].length === "number");
	}

	function getNextPieceIndex() {
	    var currentIndex = cars.getBotsCurrentPieceIndex();
	    var nextPieceIndex;

	    if ((currentIndex + 1) >= track.pieces.length) {
	        //next piece is the first piece
	        nextPieceIndex = 0;
	    } else {
	        nextPieceIndex = cars.getBotsCurrentPieceIndex() + 1;
	    }

	    return nextPieceIndex;
	}

	function getPiece(pieceIndex) {
		return track.pieces[pieceIndex];
	}

	function getRoutesLaneLengths(startPieceIndex, endPieceIndex) {
		var laneLengths = []
		for (var i = 0; i < track.lanes.length; i++) {
			laneLengths[i] = 0;
		}

		var lastPieceIndex = getLastPieceIndex();
		//route doesn't go over finish line
		if(endPieceIndex <= lastPieceIndex) {
		    for (var i = startPieceIndex; i < endPieceIndex; i++) {
		        for (var j = 0; j < track.lanes.length; j++) {
		            laneLengths[j] += getLaneLength(i, j);
		        }
		    }
		} else { //route goes over finish line
		    for (var i = startPieceIndex; i <= lastPieceIndex; i++) {
		        for (var j = 0; j < track.lanes.length; j++) {
		            laneLengths[j] += getLaneLength(i, j);
		        }
		    }

			for (var i = 0; i < endPieceIndex; i++) {
			    for (var j = 0; j < track.lanes.length; j++) {
			        laneLengths[j] += getLaneLength(i, j);
			    }
			}
		}

		return laneLengths;
	}

	function getLaneLength(pieceIndex, laneIndex) {
		var piece = track.pieces[pieceIndex];
		var pieceLength;
		if (typeof piece.length === "number") {
		    // true if piece is straight
		    pieceLength = piece.length;
		} else {
		    // false if piece is curved
		    pieceLength = getCurvedPieceLength(piece, track.lanes[laneIndex]);
		}

		return pieceLength;
	}

	function getCurvedPieceLength(piece, lane) {
		var pieceLength;
		if (typeof piece["length" + lane.index] === "number") {
		    //length already calculated
		    return piece["length" + lane.index];
		} else {
			//length isn't calculated yet for this piece and lane
			var actualRadius;
			if (piece.angle > 0) {
			    //piece is curves to the right
			    if (lane.distanceFromCenter > 0) {
			        //lane is on the right side of a piece
			        actualRadius = piece.radius - lane.distanceFromCenter;
			    } else if (lane.distanceFromCenter < 0) {
			        //lane is on the left side of a piece
			        actualRadius = piece.radius + Math.abs(lane.distanceFromCenter);
			    } else {
			        //lane is in the middle of a piece
			        actualRadius = piece.radius;
			    }
			} else {
			    //piece is curves to the left
			    if (lane.distanceFromCenter > 0) {
			        //lane is on the right side of a piece
			        actualRadius = piece.radius + lane.distanceFromCenter;
			    } else if (lane.distanceFromCenter < 0) {
			        //lane is on the left side of a piece
			        actualRadius = piece.radius - Math.abs(lane.distanceFromCenter);
			    } else {
			        //lane is in the middle of a piece
			        actualRadius = piece.radius;
			    }
			}
			pieceLength = ( Math.abs(piece.angle) / 360 ) * Math.PI * actualRadius * 2;
			piece["length" + lane.index] = pieceLength;
		}

		return pieceLength;
	}

    /*
    * When the car crashes, save the data about current speed, piece, lane and throttle. 
    * Next time try to avoid exceeding that speed on that piece.
    */
	function saveCrashReport() {
	    var currentIndex = cars.getBotsCurrentPieceIndex();
	    track.pieces[currentIndex].lastCrash = {};
	    track.pieces[currentIndex].lastCrash.speed = cars.getBotsCurrentSpeed();
	    track.pieces[currentIndex].lastCrash.piece = currentIndex;
	    track.pieces[currentIndex].lastCrash.lane = cars.getBotsCurrentLaneIndex();
	    track.pieces[currentIndex].lastCrash.throttle = cars.getBotsCurrentThrottle();

	    console.log('Crash report saved: ' + JSON.stringify(track.pieces[currentIndex].lastCrash));
	}

    /*
    * Returns the currently known max speed of a piece when the bot crashes.
    * If the bot hasn't crashed, then make a guess based on experience...
    */
	function getCrashSpeedOfPiece(pieceIndex, laneIndex) {
	    if (track.pieces[pieceIndex].hasOwnProperty('lastCrash')) {
	        return track.pieces[pieceIndex].lastCrash.speed;
	    } else {
	        return 100;
	    }
	}

	return {
	    setTrackData:           setTrackData,
        getTrackData:           getTrackData,
		getLastPieceIndex:		getLastPieceIndex,
		getLaneLength:			getLaneLength,
		getRoutesLaneLengths:	getRoutesLaneLengths,
		isPieceStraight:		isPieceStraight,
		isPieceSwitch:			isPieceSwitch,
		isNextPieceSwitch:      isNextPieceSwitch,
		isNextPieceCurve:       isNextPieceCurve,
		getNumNextStraightPieces: getNumNextStraightPieces,
		getLengthOfNextStraightPieces: getLengthOfNextStraightPieces,
		saveCrashReport:        saveCrashReport,
		getCrashSpeedOfPiece:   getCrashSpeedOfPiece,
		getNextPieceIndex:      getNextPieceIndex,
		getCurveAngle:          getCurveAngle,
		getCurveAdjustment:     getCurveAdjustment
	}
})()


/**********************************************************************************
 *
 *		UTILITIES
 *
 *********************************************************************************/

// Checks elapsed time in milliseconds and warns if it's more than 40 ms (50 ms is the limit for dqf)
var checkElapsedTime = function (note) {
    if (!logTimes) {
        return;
    }

    var elapsed = process.hrtime(startTime)[1] / 1000;

    if (elapsed > 40) {
        console.log('Time WARN: ' + elapsed + ' ms - ' + note);
    }

    startTime = process.hrtime(); // reset the timer
    return elapsed;
}

/*
var quicklog = (function() {
	var dir = "./debug/";
	var logpath = dir + constructDateTimeString() + ".log";
	var fs = require('fs');

	if( !(fs.existsSync(dir)) ) {
		console.log("Logging directory debug/ doesn't exist");
		fs.mkdir(dir, function(error) {
			console.log(error);
		});
		console.log("Directory " + dir + " created");
	}

	var fd = fs.openSync(logpath, 'a', 0666);

	function constructDateTimeString() {
		var d = new Date();

		var yyyy = d.getFullYear().toString();
		var MM = (d.getMonth()+1).toString(); // getMonth() is zero-based
		var dd = d.getDate().toString();
		var hh = d.getHours().toString();
		var mm = d.getMinutes().toString();
		var ss = d.getSeconds().toString();

		return yyyy + (MM[1]?MM:"0"+MM[0]) + (dd[1]?dd:"0"+dd[0])
			+ "-" + (hh[1]?hh:"0"+hh[0]) + (mm[1]?mm:"0"+mm[0])
			+ (ss[1]?dd:"0"+dd[0]);
	}

	function write(data) {
		var s = JSON.stringify(data, null, 4)
		fs.writeSync(fd, s + '\n');
	}

	function close() {
		fs.closeSync(fd);
	}

	return {
		write: write,
		close: close
	}
})()
*/

/**********************************************************************************
 *
 *		START
 *
 *********************************************************************************/


init();


////////////////////////////////// ----END---- //////////////////////////////////









































